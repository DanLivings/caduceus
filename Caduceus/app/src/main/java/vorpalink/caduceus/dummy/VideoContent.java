package vorpalink.caduceus.dummy;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import vorpalink.caduceus.R;
import vorpalink.caduceus.VideoPlayerActivity;

/**
 * Created by Dan on 13/01/2015.
 */
public class VideoContent {

    public static List<VideoItem> ITEMS = new ArrayList<>();

    public static Map<String, VideoItem> ITEM_MAP = new HashMap<>();

    public static void addItem(VideoItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }
    public static void clear()
    {
        ITEMS.clear();
        ITEM_MAP.clear();
    }

    public static class VideoItem {
        private String id;
        private String title;
        private String description;
        private String category;
        private String file;
        private String thumbnail;

        public VideoItem(String id, String title, String description, String category, String file, String thumbnail) {
            this.id             = id;
            this.title          = title;
            this.description    = description;
            this.category       = category;
            this.file           = file;
            this.thumbnail      = thumbnail;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        @Override
        public String toString() {
            return this.id + "," + this.title + "," + this.description + "," + this.category + "," + this.file + "," + this.thumbnail;
        }
    }

    public static class VideoItemAdapter extends ArrayAdapter<VideoItem> {
        private Context context;

        public VideoItemAdapter (Context context, List<VideoItem> videoItems) {
            super(context, 0, videoItems);

            this.context = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final VideoItem videoItem = getItem(position);

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.video_list_item, parent, false);

            TextView tvVideoId          = (TextView)        convertView.findViewById(R.id.video_id);
            ImageButton ivThumbnail     = (ImageButton)     convertView.findViewById(R.id.video_thumbnail);
            TextView tvVideoTitle       = (TextView)        convertView.findViewById(R.id.video_title);
            TextView tvVideoDesc        = (TextView)        convertView.findViewById(R.id.video_desc);

            ivThumbnail.setOnClickListener(new ClickHandler(position));
            Log.wtf("Loaded Thumbnail: ", videoItem.thumbnail + "(" + context.getResources().getIdentifier(videoItem.thumbnail, null, context.getPackageName()) + ")");
            ivThumbnail.setImageResource(context.getResources().getIdentifier(videoItem.thumbnail, null, context.getPackageName()));

            tvVideoId.setText(videoItem.id);
            tvVideoTitle.setText(videoItem.title);
            tvVideoDesc.setText(videoItem.description);

            return convertView;
        }

        private class ClickHandler implements View.OnClickListener
        {
            private int position;

            public ClickHandler(int _position)
            {
                this.position = _position;
            }

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(v.getContext(), VideoPlayerActivity.class);
                intent.putExtra("extra_video", VideoContent.ITEMS.get(position).toString());
                v.getContext().startActivity(intent);
            }
        }
    }
}
