package vorpalink.caduceus;

import android.support.v7.app.ActionBarActivity;

/**
 * Created by Tim on 12/01/2015.
 */
public class DrawerActivity extends ActionBarActivity
{
    public String onSectionAttached(int number) {
        switch (number) {
            case 1:
                return getString(R.string.title_section1);
            case 2:
                return getString(R.string.title_section2);
            case 3:
                return getString(R.string.title_section3);
            case 4:
                return getString(R.string.title_section4);
            case 5:
                return getString(R.string.title_section5);
        }

        return "UNKNOWN";
    }
}
