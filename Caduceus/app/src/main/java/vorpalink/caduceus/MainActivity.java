package vorpalink.caduceus;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import vorpalink.caduceus.datamodel.MySQLiteHelper;
import vorpalink.caduceus.datamodel.Video;
import vorpalink.caduceus.dummy.VideoContent;


public class MainActivity extends DrawerActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, VideoListFragment.OnFragmentInteractionListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private VideoListFragment videoListFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
        getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();

        videoListFragment = VideoListFragment.newInstance("" /*+ (position + 1)*/, "No idea");

        fragmentManager.beginTransaction()
                .replace(R.id.container, videoListFragment)
                .commit();

        loadVideos(0);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Log.wtf("Clearing previous content", "");

        VideoContent.clear();

        if(videoListFragment != null)
            videoListFragment.refreshList(this);

        loadVideos(position);
    }

    public void loadVideos(int position) {
        Log.wtf("Loading content for:", getResources().getStringArray(R.array.categories_array)[position]);

        String[] videos = getResources().getStringArray(R.array.videos);
        String[] videoParts;

        for(String video : videos)
        {
            videoParts = video.split(",");
            if(Integer.valueOf(videoParts[3]) == position) {
                if(videoParts.length == 6)
                    VideoContent.addItem(new VideoContent.VideoItem(videoParts[0], videoParts[1], videoParts[2], videoParts[3], videoParts[4], videoParts[5]));
                else
                    VideoContent.addItem(new VideoContent.VideoItem(videoParts[0], videoParts[1], videoParts[2], videoParts[3], videoParts[4], ""));
            }
        }

        if(videoListFragment != null)
            videoListFragment.refreshList(this);

        //setTitle(getResources().getStringArray(R.array.categories_array)[position]);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_about) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String id) {
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }
}
