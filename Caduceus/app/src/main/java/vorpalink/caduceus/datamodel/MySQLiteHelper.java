package vorpalink.caduceus.datamodel;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

public class MySQLiteHelper extends SQLiteOpenHelper
{
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "VideoDB";

    public MySQLiteHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // SQL statement to create book table
        String CREATE_VIDEO_TABLE = "CREATE TABLE " + Video.TABLE_NAME + " ( " +
                Video.ID_COLUMN             + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Video.TITLE_COLUMN          + " title TEXT, "+
                Video.DESCRIPTION_COLUMN    + " description TEXT, "+
                Video.THUMBNAIL_COLUMN      + " thumbnail TEXT, "+
                Video.FILE_COLUMN           + " file TEXT)";

        // create books table
        db.execSQL(CREATE_VIDEO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS Video");

        // create fresh books table
        this.onCreate(db);
    }

    public void addVideo(Video _video)
    {
        Log.d("addVideo", _video.toString());

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        //values.put(Video.ID_COLUMN, _video.getID());
        values.put(Video.TITLE_COLUMN, _video.getTitle());
        values.put(Video.DESCRIPTION_COLUMN, _video.getDescription());
        values.put(Video.THUMBNAIL_COLUMN, _video.getThumbnail());
        values.put(Video.FILE_COLUMN, _video.getFile());

        // 3. insert
        db.insert(Video.TABLE_NAME, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public Video getVideo(int _id){

        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. build query
        Cursor cursor =
                db.query(Video.TABLE_NAME,                      // a. table
                        Video.COLUMNS,                          // b. column names
                        " id = ?",                              // c. selections
                        new String[] { String.valueOf(_id) },    // d. selections args
                        null,                                   // e. group by
                        null,                                   // f. having
                        null,                                   // g. order by
                        null);                                  // h. limit

        // 3. if we got results get the first one
        if (cursor != null)
            cursor.moveToFirst();

        // 4. build book object
        Video video = new Video();
        video.setID(Integer.parseInt(cursor.getString(0)));
        video.setTitle(cursor.getString(1));
        video.setDescription(cursor.getString(2));
        video.setThumbnail(cursor.getString(3));
        video.setFile(cursor.getString(4));

        //log
        Log.d("getVideo("+_id+")", video.toString());

        // 5. return book
        return video;
    }

    public List<Video> getAllVideos() {
        List<Video> videos = new LinkedList<Video>();

        // 1. build the query
        String query = "SELECT  * FROM " + Video.TABLE_NAME;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        Video video = null;
        if (cursor.moveToFirst()) {
            do {
                video = new Video();
                video.setID(Integer.parseInt(cursor.getString(0)));
                video.setTitle(cursor.getString(1));
                video.setDescription(cursor.getString(2));
                video.setThumbnail(cursor.getString(3));
                video.setFile(cursor.getString(4));

                // Add book to books
                videos.add(video);
            } while (cursor.moveToNext());
        }

        Log.d("getAllVideos()", videos.toString());

        // return books
        return videos;
    }

    public int updateVideo(Video _video) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(Video.ID_COLUMN, _video.getID());
        values.put(Video.TITLE_COLUMN, _video.getTitle());
        values.put(Video.DESCRIPTION_COLUMN, _video.getDescription());
        values.put(Video.THUMBNAIL_COLUMN, _video.getThumbnail());
        values.put(Video.FILE_COLUMN, _video.getFile());

        // 3. updating row
        int i = db.update(Video.TABLE_NAME, //table
                values, // column/value
                Video.ID_COLUMN+" = ?", // selections
                new String[] { String.valueOf(_video.getID()) }); //selection args

        // 4. close
        db.close();

        return i;
    }

    public void deleteVideo(Video _video) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. delete
        db.delete(Video.TABLE_NAME, //table name
                Video.ID_COLUMN+" = ?",  // selections
                new String[] { String.valueOf(_video.getID()) }); //selections args

        // 3. close
        db.close();

        //log
        Log.d("deleteVideo", _video.toString());

    }
}
