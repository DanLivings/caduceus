package vorpalink.caduceus.datamodel;

/**
 * Created by tdvolossevich on 18/02/2015.
 */
public class Video
{
    public static final String      TABLE_NAME         = "Video";
    public static final String      ID_COLUMN          = "id";
    public static final String      TITLE_COLUMN       = "title";
    public static final String      DESCRIPTION_COLUMN = "description";
    public static final String      THUMBNAIL_COLUMN   = "thumbnail";
    public static final String      FILE_COLUMN        = "file";
    public static final String[]    COLUMNS            = { ID_COLUMN, TITLE_COLUMN, DESCRIPTION_COLUMN, THUMBNAIL_COLUMN, FILE_COLUMN };

    private int      m_id;
    private String   m_title;
    private String   m_description;
    private String   m_thumbnail;
    private String   m_file;

    public Video()
    {
        this.m_id           = 0;
        this.m_title        = "UNDEFINED";
        this.m_description  = "UNDEFINED";
        this.m_thumbnail    = "UNDEFINED";
        this.m_file         = "UNDEFINED";
    }

    public Video(String _title, String _description, String _thumbnail, String _file)
    {
        this.m_id           = 0;
        this.m_title        = _title;
        this.m_description  = _description;
        this.m_thumbnail    = _thumbnail;
        this.m_file         = _file;
    }

    public int getID()
    {
        return m_id;
    }

    public String getTitle()
    {
        return m_title;
    }

    public String getDescription()
    {
        return m_description;
    }

    public String getThumbnail()
    {
        return m_thumbnail;
    }

    public String getFile()
    {
        return m_file;
    }

    public void setID(int m_id) {
        this.m_id = m_id;
    }

    public void setTitle(String m_title) {
        this.m_title = m_title;
    }

    public void setDescription(String m_description) {
        this.m_description = m_description;
    }

    public void setThumbnail(String m_thumbnail) {
        this.m_thumbnail = m_thumbnail;
    }

    public void setFile(String m_file) {
        this.m_file = m_file;
    }

    @Override
    public String toString()
    {
        return "ID:" + this.m_id + ", Title:" + this.m_title + ", Description:" + this.m_description + ", Thumbnail:" + this.m_thumbnail + ", File: " + this.m_file;
    }
}
